#include <string>
#include <cmath>
#include <iostream>
#include <ostream>
void IndividualValues(std::string value, std::string shape ,float result)
{
	std::cout << "The " << value << " of the " << shape << " shape = " << result << std::endl;
}
void PromptValueOf(std::string object, std::string function)
{
	std::cout << "Which " << object << " would you like to find the " << function << " of?" << std::endl;
}
void MenuEntry(std::string option, int value)
{
	std::cout << "[" << value << "] " << option << std::endl;
}
float radiusValue(std::string number, std::string shape)
{
    float radiusLength;
	std::cout << "What is the " << number << "base length of your " << shape << std::endl;
	scanf("%f", &radiusLength);
    return radiusLength;
}
float Value(std::string number, std::string shape)
{
	float value;
	std::cout << "What is the " << number << " of your " << shape << "?" << std::endl;
	scanf("%f", &value);
	return value;
}
void ValueOf(std::string type, std::string shape, float value)
{
	std::cout << "\n The " << type << " of your " << shape << " is " << value << " units squared. \n" << std::endl;
}
void squareArea()
{
	float sideLength;
	float squareArea;

	sideLength = Value("", "square");

	squareArea = sideLength * sideLength;
	ValueOf("area", "square", squareArea);

}
void parallelogramArea()
{
	float sideLength1, sideLength2;

	sideLength1 = Value("first "	, "parallelogram");
	sideLength2 = Value("second "	, "parallelogram");

	ValueOf("area", "parallelogram", (sideLength1 * sideLength2));
}
void righttriangleArea()
{
	float sideLength1, sideLength2;
	float righttriangleArea;

	sideLength1 = Value("first "	, "right triangle");
	sideLength2 = Value("second "	, "right triangle");

	ValueOf("area", "right triangle", (sideLength1 * sideLength2)/2);
}
void circleArea()
{
	float radiusLength;
	float pi = 3.1415;

	radiusLength = radiusValue("", "circle");
	ValueOf("area", "circle", (pi * (radiusLength * radiusLength)));
}
void sphereArea()
{
	float radiusLength;
	float pi = 3.1415;

	radiusLength = radiusValue("", "sphere");
	ValueOf("area", "sphere", 4 * (pi * (radiusLength * radiusLength)));
}
void RectangularPrismArea()
{
	float lengthValue, widthValue, heightValue, shape1, shape2, shape3, Area, Volume;

	lengthValue = Value("first ", "rectangular prism");
	widthValue = Value("second ", "rectangular prism");
	heightValue = Value("third ", "rectangular prism");

	shape1 =	lengthValue		*	widthValue;
	shape2 =	widthValue		*	heightValue;
	shape3 =	lengthValue		*	heightValue;

	Area 	= (shape1 + shape2 + shape3)*2;
	Volume 	= shape1 * heightValue;

	IndividualValues("area", "first" 	, shape1);
	IndividualValues("area", "second"	, shape2);
	IndividualValues("area", "third"	, shape3);

	ValueOf("area"	, "rectangular prism", Area);
	ValueOf("volume", "rectangular prism", Volume);
}
void TriangularPrismArea()
{
	float base1, base2, base3, height, s, baseArea, Area;

	base1 	= Value("first "	, "triangular prism");
	base2 	= Value("second "	, "triangular prism");
	base3 	= Value("third "	, "triangular prism");
	height 	= Value("height "	, "triangular prism");

	s		 = (base1 + base2 + base3)/2;
	baseArea = sqrt(s * (s - base1) * (s - base2) * (s - base3));
	Area = 2 * baseArea + (base1 + base2 + base3) * height;

	IndividualValues("area", "base" 		, baseArea);
	ValueOf("area", "triangular prism", Area);
	// ValueOf("volume", "triangular prism", Volume);) // Not implemented yet.
}
void RightTriangularPrismArea()
{
    float lengthValue, widthValue, depthValue, shape1, shape2, shape3, shape4, Area, Volume ,hypotenuse;

	lengthValue = Value("length "	, "right angle prism");
	widthValue 	= Value("width "	, "right angle prism");
    depthValue	= Value("depth "	, "right angle prism");
	hypotenuse	= sqrt(lengthValue * lengthValue +  widthValue * widthValue);

	std::cout << "The hypotenuse of your shape is " << hypotenuse << " units per the Pythagorean Theorem." << std::endl;

	shape1 = lengthValue 	* 	widthValue;
	shape2 = widthValue		* 	depthValue;
	shape3 = hypotenuse		* 	depthValue;
	shape4 = lengthValue	* 	depthValue;

	Volume = (widthValue)/2 * lengthValue * lengthValue;
	Area = shape1 + shape2 + shape3 + shape4;

	IndividualValues("area", "first" 		, shape1);
	IndividualValues("area", "second" 		, shape2);
	IndividualValues("area", "third" 		, shape3);
	IndividualValues("area", "fourth" 		, shape4);

	ValueOf("area", "right triangle prism", Area);
	ValueOf("volume", "right triangle prism", Volume);
}
void pyramidArea()
{
	float lengthValue, widthValue, heightValue, part1, part2, part3;

	lengthValue	 = 	Value("length "	, 	"pyramid");
	widthValue	 = 	Value("width "	,	"pyramid");
	heightValue	 = 	Value("height "	,	"pyramid");

	part1 = lengthValue * widthValue;
	part2 = lengthValue * (sqrt(widthValue/2 * widthValue/2 + pow(2, heightValue)));
	part3 = widthValue * (sqrt(lengthValue/2 * lengthValue/2 + (heightValue * heightValue)));

	ValueOf("area", "pyramid", part1 + part2 + part3);
}
void prismArea()
{
	int inputValue;

	while(1 == 1)
	{
		PromptValueOf("prism", "area");

		MenuEntry("Rectangular Prism", 				1	);
		MenuEntry("Triangular Prism", 				2	);
		MenuEntry("Right Angle Triangular Prism", 	3	);
		MenuEntry("Pyramid", 						4	);

		scanf("%d", &inputValue);

		if (inputValue == 1)
		{
			RectangularPrismArea();
			return;
		}

		if (inputValue == 2)
		{
			TriangularPrismArea();
			return;
		}

		if (inputValue == 3)
		{
			RightTriangularPrismArea();
			return;
		}

		if (inputValue == 4)
		{
			pyramidArea();
			return;
		}
	}
}
void AreaVolumeMenu()
{
	int inputValue;

	while(1 == 1)
	{
		PromptValueOf("object", "area");

		MenuEntry("Square", 						1	);
		MenuEntry("Rectangule / Parallelogram", 	2	);
		MenuEntry("Right Triangle", 				3	);
		MenuEntry("Circle", 						4	);
		MenuEntry("Sphere", 						5	);
		MenuEntry("Prisms", 						6	);

		scanf("%d", &inputValue);

		if (inputValue == 1)
		{
			printf("Selecting Square..."
					"\n"					);

			squareArea();
			return;
		}

		else if (inputValue == 2)
		{
			printf("Selecting Rectangle..."
					"\n"					);

			parallelogramArea();
			return;
		}

		else if (inputValue == 3)
		{
			printf("Selecting Right Triangle..."
					"\n"							);

			righttriangleArea();
			return;
		}

		else if (inputValue == 4)
		{
			printf("Selecting Circle..."
					"\n"					);

			circleArea();
			return;
		}


		else if (inputValue == 5)
		{
			printf("Selecting Sphere..."
					"\n"					);

			sphereArea();
			return;
		}

		else if (inputValue == 6)
		{
			printf("Selecting Prism..."
					"\n"				);

			prismArea();
			return;
		}
		else
		{
			printf("Invalid input."
					"\n"			);
			return;
		}
	}
}
void PythagoreanHypotenuse()
{
	float firstAdjacent, secondAdjacent, hypotenuse;

	firstAdjacent	= Value("first adjacent side", "triangle");
	secondAdjacent	= Value("second adjacent side", "triangle");
	hypotenuse 		= sqrt(pow(firstAdjacent, 2) + pow(secondAdjacent, 2));

	ValueOf("value", "hypotenuse", hypotenuse);
}
void PythagoreanTheorem()
{
	int inputValue;
	while (1 == 1);
	{
		printf("Which type of side do you want to find out?");

		MenuEntry("Hypotenuse" 			, 1);
		MenuEntry("Adjacent side"		, 2);

		if (inputValue == 1)
		{
			PythagoreanHypotenuse();
		}
	}
}
void Sin()
{
	int 	inputValue;
	float 	Value, sideValue, angleValue, sinOf, degrees, angleValueRadians, pi;
	int   	hypotenuse;
	std::string side;

	printf("What side do you want to find the value of? \n");

	MenuEntry("Hypotenuse"	, 1);
	MenuEntry("Opposite"	, 2);

	scanf("%d", &inputValue);

	if (inputValue == 1)
	{
		side = "opposite";
		hypotenuse = 1;

	}
	else if (inputValue == 2)
	{
		side = "hypotenuse";
		hypotenuse = 0;
	}
	else
	{
		printf("Invalid input.\n");
	}

	std::cout << "What is the value of your " << side << "?" << std::endl;
	scanf("%f", &sideValue);

	printf("What is the angle value that corresponds with your function?");
	scanf("%f", &angleValue);

	pi = 3.1415;

	angleValueRadians = (angleValue / 180) * pi;

	sinOf = (sin(angleValueRadians));

	if (hypotenuse == 0)
	{
		Value = sideValue * sinOf;
	}
	else
	{
		Value = sideValue / sinOf;
	}

	std::cout << "The value of your " << side << " = " << Value << std::endl;
}
void ArcSin()
{
	int inputValue;
	float value, opposite, hypotenuse, output, degrees;

	opposite 	= Value("Value", "opposite angle");
	hypotenuse	= Value("Value", "hypotenuse angle");

	output = asin(opposite/hypotenuse);

	degrees = output * 57.2958;

	std::cout << "The angular value represented by your input is " << degrees << "." << std::endl;
}
void Cosine()
{
	int inputValue;
	float Value, sideValue, angleValue, cosineOf, degrees, pi , angleValueRadians;
	int   hypotenuse;

	std::string side;

	printf("What side do you want to find the value of? \n");

	MenuEntry("Adjacent"	, 1);
	MenuEntry("Hypotenuse"	, 2);

	scanf("%d", &inputValue);

	if (inputValue == 1)
	{
		side = "hypotenuse";
		hypotenuse = 0;

	}
	else if (inputValue == 2)
	{
		side = "adjacent";
		hypotenuse = 1;
	}
	else
	{
		printf("Invalid input.\n");
	}

	std::cout << "What is the value of your " << side << "?" << std::endl;
	scanf("%f", &sideValue);

	printf("What is the angle value that corresponds with your function?");
	scanf("%f", &angleValue);

	pi = 3.1415;

	angleValueRadians = (angleValue / 180) * pi;

	cosineOf = (cos(angleValueRadians));

	if (hypotenuse == 0)
	{
		Value = sideValue * cosineOf;
	}
	else
	{
		Value = sideValue / cosineOf;
	}

	std::cout << "The value of your " << side << " = " << Value << std::endl;
}
void ArcCosine()
{
	int inputValue;
	float value, adjacent, hypotenuse, output, degrees;

	adjacent = Value("Value", "adjacent angle");
	hypotenuse = Value("Value", "hypotenuse angle");
	output = acos(adjacent/hypotenuse);
	degrees = output * 57.2958;

	std::cout << "The angular value represented by your input is " << degrees << "." << std::endl;
}
void Tangent()
{
	int inputValue, Value, sideValue, angleValue, tanOf, degrees, angleValueRadians, pi;
	int opposite;
	std::string side;

	printf("What side do you want to find the value of? \n");

	MenuEntry("Opposite"	, 1);
	MenuEntry("Adjacent"	, 2);

	scanf("%d", &inputValue);

	if (inputValue == 1)
	{
		side = "adjacent";
		opposite = 1;

	}
	else if (inputValue == 2)
	{
		side = "opposite";
		opposite = 0;
	}
	else
	{
		printf("Invalid input.\n");
	}

	std::cout << "What is the value of your " << side << "?" << std::endl;
	scanf("%f", &sideValue);
	printf("What is the angle value that corresponds with your function?\n");
	scanf("%f", &angleValue);

	pi = 3.1415;
	angleValueRadians = (angleValue / 180) * pi;
	tanOf = (tan(angleValueRadians));

	if (opposite == 0)
	{
		Value = sideValue * tanOf;
	}
	else
	{
		Value = sideValue / tanOf;
	}

	std::cout << "The value of your " << side << " = " << Value << std::endl;
}
void ArcTangent()
{
	int inputValue;
	float value, opposite, adjacent, output, degrees;

	opposite = Value("Value", "opposite angle");
	adjacent = Value("Value", "adjacent angle");
	output = atan(opposite/adjacent);
	degrees = output * 57.2958;

	std::cout << "The angular value represented by your input is " << degrees << "." << std::endl;
}
void Trigonometry()
{
	int inputValue;
	while (1 == 1)
	{
		printf("What trigonometric equation would you like to use?\n");

		MenuEntry("Sin"				, 1);
		MenuEntry("Inverse Sin"		, 2);
		MenuEntry("Cosine"			, 3);
		MenuEntry("Inverse Cosine"	, 4);
		MenuEntry("Tangent"			, 5);
		MenuEntry("Inverse Tangent"	, 6);

		scanf("%d", &inputValue);

		if (inputValue == 1)
			Sin();
		else if (inputValue == 2)
			ArcSin();

		else if (inputValue == 3)
			Cosine();
		else if (inputValue == 4)
			ArcCosine();

		else if (inputValue == 5)
			Tangent();
		else if (inputValue == 6)
			ArcTangent();
	}
}
void RightTriangleTheoremsMenu()
{
	int inputValue;
	while (1 == 1)
	{
		printf("\n What equation would you like use? \n \n");

		MenuEntry("Pythagorean Theorem", 1);
		MenuEntry("Trigonometry"		, 2);

		scanf("%d", &inputValue);

		if (inputValue == 1)
		{
			printf("Selecting Pythagorean Theorem...");
			PythagoreanTheorem();
		}
		else if (inputValue == 2)
			Trigonometry();
	}
}

int main()
{

	int inputValue;
	while (1 == 1)
	{

		printf("\n Which value would you like to find out? \n \n");

		MenuEntry("Volume / Area", 				1	);
		MenuEntry("Perimeter", 					2	);
		MenuEntry("Volume", 					3	);
		MenuEntry("Right Triangle Theorems", 	4	);

		scanf("%d", &inputValue);

		if (inputValue == 1)
		{
			printf("Selecting Area..."
					"\n"				);
			AreaVolumeMenu();
		}

		else if (inputValue == 2)
			printf("Selecting Perimeter..."
					"\n"					);

		else if (inputValue == 4)
		{
			printf("Selecting Right Triangle Theorems..."
					"\n"									);
			RightTriangleTheoremsMenu();
		}

		else
			printf("Invalid input."
					"\n"			);
	}
}
